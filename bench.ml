let nb_iters = ref 40

let nb_worker_domains = ref 1
let nb_idle_domains = ref 0

let () =
  let open Arg in
  let anon_fun arg = raise (Bad arg) in
  Arg.parse [
    "--nb-iters",
      Set_int nb_iters,
      Printf.sprintf " number of benchmark iterations (default %d)" !nb_iters;
    "--nb-idle-domains",
      Set_int nb_idle_domains,
      " (default 0)";
    "--nb-worker-domains",
      Set_int nb_worker_domains,
      " (default 1)";
  ] anon_fun "";
  if !nb_worker_domains < 1 then
    raise (Bad "nb-worker-domains must be at least 1");
  ()


let () =

  (* start idle domains *)
  let idle_run = Atomic.make true in
  let idle_domains =
    let idle () =
      while Atomic.get idle_run do
        List.init 10 (fun _ -> ())
        |> Sys.opaque_identity
        |> ignore;
        Unix.sleepf 1E-5
      done
    in
    Array.init !nb_idle_domains
      (fun _ -> Domain.spawn idle)
  in

  let work () =
    for _ = 1 to !nb_iters do
      Kbmain.run ()
    done
  in

  (* start worker domains + work on this domain *)
  let worker_domains =
    (* the current domain will also work, spawn one domain less *)
    Array.init (!nb_worker_domains - 1)
      (fun _ -> Domain.spawn work)
  in
  work ();

  (* release idle domains *)
  Atomic.set idle_run false;

  (* join all domains *)
  Array.iter Domain.join idle_domains;
  Array.iter Domain.join worker_domains;
