(cd misc-kb; sh build.sh || exit 1)
$OCAMLOPT -I misc-kb misc-kb/{terms,equations,orderings,kb,kbmain}.cmx bench.ml -o bench


# my usage:
# BUILD=/home/gasche/Prog/ocaml/github-trunk OCAMLOPT="$BUILD/runtime/ocamlrun $BUILD/ocamlopt -nostdlib -I $BUILD/stdlib" sh build.sh
