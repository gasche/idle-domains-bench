let recommended_domain_count = Domain.recommended_domain_count ()

type result = {
  time: float;
  max_rss_kbytes: int;
}

let time_format : string =
  (* %e: elapsed real tnb_ime in seconds
     %M: maximum resident set size in KB *)
  "time: %es; memory: %MKB"

let parse_result input =
  Scanf.sscanf input
    "time: %fs; memory: %dKB"
    (fun time max_rss_kbytes -> { time; max_rss_kbytes })

let print_result fmt { time; max_rss_kbytes; } =
  Printf.fprintf fmt
    "time: %fs; memory: %dKB"
    time
    max_rss_kbytes

let bench_cmd = "./bench"
let time_out = "time.txt"

let nb_iters = 100

let time_cmd ~nb_workers ~nb_idle =
  Printf.sprintf
    "/bin/time --output %s --format=\"%s\" \
       %s --nb-iters %d --nb-worker-domains %d --nb-idle-domains %d"
    time_out time_format
    bench_cmd nb_iters nb_workers nb_idle

let time_result ~nb_workers ~nb_idle =
  let cmd = time_cmd ~nb_workers ~nb_idle in
  assert (Sys.command cmd = 0);
  In_channel.with_open_text time_out @@ fun ic ->
  let out = In_channel.input_all ic in
  parse_result out

let compare ~reference ~result ~nb_workers ~nb_idle =
  let expected_time =
    reference.time *. float (1 + nb_workers / recommended_domain_count) in
  let expected_memory =
    float reference.max_rss_kbytes *. float nb_workers
  in
  let time_slowdown =
    result.time /. expected_time in
  let memory_blowup =
    float result.max_rss_kbytes /. expected_memory in
  (time_slowdown, memory_blowup)

let () =
  let reference = time_result ~nb_workers:1 ~nb_idle:0 in
  for nb_workers = 1 to recommended_domain_count + 3 do
    Printf.printf "w%02d:" nb_workers;
    [0; 1; 2; 5; 10] |> List.iter begin fun nb_idle ->
      let result = time_result ~nb_workers ~nb_idle in
      let (time_slowdown, memory_blowup) =
        compare ~reference ~result ~nb_workers ~nb_idle
      in
      Printf.printf
        (* " [i%02d %.2fs (x%.2f) %dKB (x%.2f)]%!" *)
        " [i%02d %.2fs (x%.2f)]%!"
        nb_idle
        result.time time_slowdown
        (* result.max_rss_kbytes memory_blowup *)
    end;
    print_newline ()
  done
